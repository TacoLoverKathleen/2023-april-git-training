---
title: How to make your dog internet famous
date: 2022-09-28
draft: false
type: blog
lastmod: 2022-09-28
description: Teach your dog to fetch... social media followers!
preview: /uploads/husky-with-camera.jpg
---

I never realized that managing your pet's social media presence could actually be a viable (and potentially lucrative!) career move until reading a recent [Dear Prudence advice column](https://slate.com/human-interest/2022/07/dear-prudence-petfluencer-decision-time.html). But, apparently, it's actually a legitimate business opportunity.

Pets who are internet famous, also known as petfluencers, can bring in real income for their owners. For example, a Pomeranian Spitz known as [@jiffprom](https://www.instagram.com/jiffpom/) has 10 million followers and can fetch an average of $23,900 *per video* on TikTok.

Maybe you have big dreams of starting a side gig that involves taking pictures of your adorable puppy all day. Or maybe you just want to brighten up the daily feeds of your friends and family members with the [occasional cute doggie post](https://www.reddit.com/r/aww/). Either way, read this blog entry to learn some effective strategies to boost your pet's social media profile.


## Choose a theme

It might help to take some time to think through what your dog's X-factor is going to be: that special thing that is unique to your dog that can make it really stand out from the crowd. Perhaps it is your dog's charming personality, quirky antics, or physical attributes. For example, the famous mini-dachshunds [Gretel and Summit](https://www.instagram.com/youdidwhatwithyourwiener/) are always photographed climbing mountains in beautiful natural landscapes.

## Post regularly

You shouldn't feel like you need to post 30 times a day. I assure you that no one needs to see your dog *that* much. Perhaps try to post approximately 1-2 posts a day. If that is a little too much as you're first starting out, try by posting 1-2 times a week, then gradually increase your frequency from there.


## Learn how to take great photos

To enhance your dog's natural cuteness even more, you might want to invest some time in learning how to be a better photographer and photo editor. Your local community college likely offers inexpensive photography courses where you can learn more about composition techniques and how to properly light your photographs.

When it comes to photo editing, there are a wide variety of apps, filters, and software that are relatively easy to learn and use. Watching videos at one of the Internet's many skill-sharing websites can be a great way to improve your photo editing abilities.


## Write great captions

If your cute pictures aren't getting the level of engagement you'd like to see on their own, consider whether you could improve the captions that accompany your pictures. Consider writing captions are that:

Funny
Heartwarming
Tell a story
Highlight your dog's winning personality

For inspiration, try reading this list of [110 dog instragram captions](https://www.k9ofmine.com/dog-instagram-captions/).


---

Whether your dog's social media account becomes successful or not, never forget that it's important to have fun. Always remember to use these photo sessions as a chance to bond with your pet and shower your dog with the love and attention that it deserves from you.